From 96610c6cfa796dc15c5afcf0fd9f9b75869827fe Mon Sep 17 00:00:00 2001
From: Kevin McCarthy <kevin@8t8.us>
Date: Sun, 3 Sep 2023 12:22:01 +0800
Subject: [PATCH] Fix rfc2047 base64 decoding to abort on illegal characters.

For some reason, the rfc2047 base64 decoder ignored illegal
characters, instead of aborting.  This seems innocuous, but in fact
leads to at least three crash-bugs elsewhere in Mutt.

These stem from Mutt, in some cases, passing an entire header
field (name, colon, and body) to the rfc2047 decoder.  (It is
technically incorrect to do so, by the way, but is beyond scope for
these fixes in stable).  Mutt then assumes the result can't be empty
because of a previous check that the header contains at least a colon.

This commit takes care of the source of the crashes, by aborting the
rfc2047 decode.  The following two commits add protective fixes to the
specific crash points.

Thanks to Chenyuan Mi (@morningbread) for discovering the strchr
crashes, giving a working example draft message, and providing the
stack traces for the two NULL derefences.

(cherry picked from commit 452ee330e094bfc7c9a68555e5152b1826534555)
---
 rfc2047.c | 2 +-
 1 file changed, 1 insertion(+), 1 deletion(-)

diff --git a/rfc2047.c b/rfc2047.c
index 1ce82ebb..36cc76db 100644
--- a/rfc2047.c
+++ b/rfc2047.c
@@ -724,7 +724,7 @@ static int rfc2047_decode_word (BUFFER *d, const char *s, char **charset)
 	    if (*pp == '=')
 	      break;
 	    if ((*pp & ~127) || (c = base64val(*pp)) == -1)
-	      continue;
+              goto error_out_0;
 	    if (k + 6 >= 8)
 	    {
 	      k -= 2;
